package Model;

public class Pessoa {
	
	private Dados dado;

	public Pessoa(Dados dado) {
		super();
		this.dado = dado;
	}
		
	
	public Dados getDado() {
		return dado;
	}

	public void setDado(Dados dado) {
		this.dado = dado;
	}


}
