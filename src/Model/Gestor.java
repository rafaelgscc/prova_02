package Model;

public class Gestor extends Moderador {

	private String orgao;

	public Gestor(String nomeUsuario, String email, String senha,
			String moderacao, String orgao) {
		super(nomeUsuario, email, senha, moderacao);
		this.orgao = orgao;
	}

	public String getOrgao() {
		return orgao;
	}

	public void setOrgao(String orgao) {
		this.orgao = orgao;
	}
	
	
}
